# Egzamin

Zadanie na "zaliczenie" z przedmiotu "Programowanie aplikacji internetowych".
Aplikacja internetowa (webowa) to aplikacji, która nie jest instalowana na Twoim komputerze ale uruchamia sie i działa w internecie, na przeglądarce internetowej.Aplikacje takie w pewnym senxie są zainstalowane na serwerze, a użytkownik dostajesz do nich dostęp za pośrednictwem internetu. Elementem niezbędnym do działania aplikacji internetowej jest serwer, który udostępnia uzytkownikowi treści.
## Język aplikacji webowej 
Aplikacje webowe mogą być napisane w wielu językach (może to być Ruby, Python, PHP czy Java i wiele innych). Języków jest bardzo dużo, jednak mechanizm działania jest zawsze bardzo podobny.

Aplikacja webowa napisana w dowolnym języku interpretuje żądanie wysłane przez przeglądarkę użytkownika do serwera i odpowiada na nie generując odpowiednią zawartość. Zawartość to plik generowany dynamicznie, który jest zrozumiały przez przeglądarkę internetową.
Taki sposób komunikacji pozwala na zastosowanie praktycznie dowolnego języka. W takiej sytuacji mówimy o protokole komunikacji. Protokole czyli zbiorze reguł, których przestrzeganie pozwala na porozumienie pomiędzy przeglądarką a serwerem.
Protokół ten używany jest w komunikacji pomiędzy klientem a serwerem. W tym przypadku klientem jest przeglądarka internetowa a serwerem jest aplikacja, która przetwarza żądanie wysłane przez klienta. W takim przypadku często też mówimy o aplikacjach typu klient-serwer.

Język, który rozumie przeglądarka
## Project status

